using Microsoft.Extensions.Configuration;
using Castle.MicroKernel.Registration;
using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using WorkflowCoreDockingDemo.Configuration;
using WorkflowCoreDockingDemo.EntityFrameworkCore;
using WorkflowCoreDockingDemo.Migrator.DependencyInjection;

namespace WorkflowCoreDockingDemo.Migrator
{
    [DependsOn(typeof(WorkflowCoreDockingDemoEntityFrameworkModule))]
    public class WorkflowCoreDockingDemoMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public WorkflowCoreDockingDemoMigratorModule(WorkflowCoreDockingDemoEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbSeed = true;

            _appConfiguration = AppConfigurations.Get(
                typeof(WorkflowCoreDockingDemoMigratorModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                WorkflowCoreDockingDemoConsts.ConnectionStringName
            );

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(
                typeof(IEventBus), 
                () => IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                )
            );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WorkflowCoreDockingDemoMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
