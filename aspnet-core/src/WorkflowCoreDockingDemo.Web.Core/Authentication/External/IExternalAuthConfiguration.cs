﻿using System.Collections.Generic;

namespace WorkflowCoreDockingDemo.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
