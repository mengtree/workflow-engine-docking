using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace WorkflowCoreDockingDemo.Controllers
{
    public abstract class WorkflowCoreDockingDemoControllerBase: AbpController
    {
        protected WorkflowCoreDockingDemoControllerBase()
        {
            LocalizationSourceName = WorkflowCoreDockingDemoConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
