﻿using Abp.AutoMapper;
using WorkflowCoreDockingDemo.Authentication.External;

namespace WorkflowCoreDockingDemo.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
