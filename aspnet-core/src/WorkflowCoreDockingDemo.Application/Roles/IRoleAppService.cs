﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using WorkflowCoreDockingDemo.Roles.Dto;

namespace WorkflowCoreDockingDemo.Roles
{
    public interface IRoleAppService : IAsyncCrudAppService<RoleDto, int, PagedRoleResultRequestDto, CreateRoleDto, RoleDto>
    {
        Task<ListResultDto<PermissionDto>> GetAllPermissions();

        Task<GetRoleForEditOutput> GetRoleForEdit(EntityDto input);

        Task<ListResultDto<RoleListDto>> GetRolesAsync(GetRolesInput input);
        Task<List<GetKeyValueInfoOutput>> GetRoles4Workflow();
        Task<List<GetKeyValueInfoOutput>> GetUserByRoleId4Workflow(UserSelectorInput input);
        Task<object> GetUserSelections4Workflow();
        Task<object> GetUsers4Workflow(UserSelectorInput input);
    }
    public class UserSelectorInput
    {
        public string SelectionId { get; set; }
    }
    public class GetKeyValueInfoOutput
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
