using System.ComponentModel.DataAnnotations;

namespace WorkflowCoreDockingDemo.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}