﻿using Abp.Application.Services;
using WorkflowCoreDockingDemo.MultiTenancy.Dto;

namespace WorkflowCoreDockingDemo.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

