﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WorkflowCoreDockingDemo.Authorization.Accounts.Dto;
using static WorkflowCoreDockingDemo.Authorization.Accounts.AccountAppService;

namespace WorkflowCoreDockingDemo.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
        Task<WorkflowLoginViewModel> WorkflowLogin();
    }
}
