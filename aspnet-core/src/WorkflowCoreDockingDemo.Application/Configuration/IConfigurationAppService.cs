﻿using System.Threading.Tasks;
using WorkflowCoreDockingDemo.Configuration.Dto;

namespace WorkflowCoreDockingDemo.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
