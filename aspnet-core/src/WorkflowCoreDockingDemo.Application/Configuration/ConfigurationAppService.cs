﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using WorkflowCoreDockingDemo.Configuration.Dto;

namespace WorkflowCoreDockingDemo.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : WorkflowCoreDockingDemoAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
