﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WorkflowCoreDockingDemo.Sessions.Dto;

namespace WorkflowCoreDockingDemo.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
