﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCoreDockingDemo.Workflows.Dto;

namespace WorkflowCoreDockingDemo.Workflows
{
    public class WorkflowsAppService : IWorkflowsAppService
    {
        private readonly WorkflowManager _workflowManager;

        public WorkflowsAppService(WorkflowManager workflowManager)
        {
            _workflowManager = workflowManager;
        }
        [AllowAnonymous]
        public async Task HandleStatusChange(TaskStateChangeDataDto taskStateChangeData)
        {
            await _workflowManager.HandleStatusChange(taskStateChangeData);
        }
    }
}
