﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCoreDockingDemo.Workflows.Dto;

namespace WorkflowCoreDockingDemo.Workflows
{
    public interface IWorkflowsAppService:IApplicationService
    {
        Task HandleStatusChange(TaskStateChangeDataDto taskStateChangeData);
    }
}
