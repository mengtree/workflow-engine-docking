using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using WorkflowCoreDockingDemo.CustomForm.LeaveForms.Dto;
using WorkflowCoreDockingDemo.Roles.Dto;
using WorkflowCoreDockingDemo.Users.Dto;

namespace WorkflowCoreDockingDemo.Users
{
    public interface ILeaveFormAppService : IAsyncCrudAppService<LeaveFormDto, long, PagedResultRequestDto, CreateLeaveFormDto, UpdateLeaveFormDto>
    {
    }
}
