﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCoreDockingDemo.Workflows;

namespace WorkflowCoreDockingDemo.CustomForm.LeaveForms.Dto
{
    [AutoMapFrom(typeof(LeaveForm))]
    public class LeaveFormDto:EntityDto<long>
    {
        /// <summary>
        /// 天数
        /// </summary>
        public int Days { get; set; }
        /// <summary>
        /// 原因
        /// </summary>
        public string Reason { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }

        public DateTime UpdateTime { get; set; }
        public WorkTaskStatus WorkflowStatus { get; set; }
        public DateTime? WorkflowStatusUpdateTime { get; set; }
    }
}
