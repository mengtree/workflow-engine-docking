﻿using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Threading.BackgroundWorkers;
using WorkflowCoreDockingDemo.Authorization;
using WorkflowCoreDockingDemo.Workflows;

namespace WorkflowCoreDockingDemo
{
    [DependsOn(
        typeof(WorkflowCoreDockingDemoCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class WorkflowCoreDockingDemoApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<WorkflowCoreDockingDemoAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(WorkflowCoreDockingDemoApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );

            IocManager.Register<SyncWorkStatusBackgroundWorker>();
        }

        public override void PostInitialize()
        {

            var workManager = IocManager.Resolve<IBackgroundWorkerManager>();
            workManager.Add(IocManager.Resolve<SyncWorkStatusBackgroundWorker>());
        }
    }
}
