﻿using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.EntityFrameworkCore;
using WorkflowCoreDockingDemo.EntityFrameworkCore.Seed;

namespace WorkflowCoreDockingDemo.EntityFrameworkCore
{
    [DependsOn(
        typeof(WorkflowCoreDockingDemoCoreModule), 
        typeof(AbpZeroCoreEntityFrameworkCoreModule))]
    public class WorkflowCoreDockingDemoEntityFrameworkModule : AbpModule
    {
        /* Used it tests to skip dbcontext registration, in order to use in-memory database of EF Core */
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<WorkflowCoreDockingDemoDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        WorkflowCoreDockingDemoDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        WorkflowCoreDockingDemoDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WorkflowCoreDockingDemoEntityFrameworkModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            if (!SkipDbSeed)
            {
                SeedHelper.SeedHostDb(IocManager);
            }
        }
    }
}
