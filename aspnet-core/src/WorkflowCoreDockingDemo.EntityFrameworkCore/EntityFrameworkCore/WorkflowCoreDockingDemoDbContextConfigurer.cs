using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace WorkflowCoreDockingDemo.EntityFrameworkCore
{
    public static class WorkflowCoreDockingDemoDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<WorkflowCoreDockingDemoDbContext> builder, string connectionString)
        {
            builder.UseMySql(connectionString, new MySqlServerVersion("8.0.26"));
        }

        public static void Configure(DbContextOptionsBuilder<WorkflowCoreDockingDemoDbContext> builder, DbConnection connection)
        {
            builder.UseMySql(connection, new MySqlServerVersion("8.0.26"));
        }
    }
}
