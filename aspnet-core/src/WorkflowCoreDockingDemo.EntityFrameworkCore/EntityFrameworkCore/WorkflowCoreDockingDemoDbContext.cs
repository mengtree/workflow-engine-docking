﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using WorkflowCoreDockingDemo.Authorization.Roles;
using WorkflowCoreDockingDemo.Authorization.Users;
using WorkflowCoreDockingDemo.MultiTenancy;
using WorkflowCoreDockingDemo.CustomForm;

namespace WorkflowCoreDockingDemo.EntityFrameworkCore
{
    public class WorkflowCoreDockingDemoDbContext : AbpZeroDbContext<Tenant, Role, User, WorkflowCoreDockingDemoDbContext>
    {
        /* Define a DbSet for each entity of the application */

        public DbSet<LeaveForm> LeaveForms { get; set; }
        public WorkflowCoreDockingDemoDbContext(DbContextOptions<WorkflowCoreDockingDemoDbContext> options)
            : base(options)
        {
        }
    }
}
