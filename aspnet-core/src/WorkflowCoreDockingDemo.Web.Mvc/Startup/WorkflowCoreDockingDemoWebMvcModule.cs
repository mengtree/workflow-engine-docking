﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using WorkflowCoreDockingDemo.Configuration;

namespace WorkflowCoreDockingDemo.Web.Startup
{
    [DependsOn(typeof(WorkflowCoreDockingDemoWebCoreModule))]
    public class WorkflowCoreDockingDemoWebMvcModule : AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public WorkflowCoreDockingDemoWebMvcModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<WorkflowCoreDockingDemoNavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WorkflowCoreDockingDemoWebMvcModule).GetAssembly());
        }
    }
}
