using System.Collections.Generic;
using WorkflowCoreDockingDemo.Roles.Dto;

namespace WorkflowCoreDockingDemo.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
