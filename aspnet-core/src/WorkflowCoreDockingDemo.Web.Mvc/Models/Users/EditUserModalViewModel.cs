using System.Collections.Generic;
using System.Linq;
using WorkflowCoreDockingDemo.Roles.Dto;
using WorkflowCoreDockingDemo.Users.Dto;

namespace WorkflowCoreDockingDemo.Web.Models.Users
{
    public class EditUserModalViewModel
    {
        public UserDto User { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }

        public bool UserIsInRole(RoleDto role)
        {
            return User.RoleNames != null && User.RoleNames.Any(r => r == role.NormalizedName);
        }
    }
}
