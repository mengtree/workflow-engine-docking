﻿using Abp.AutoMapper;
using WorkflowCoreDockingDemo.Roles.Dto;
using WorkflowCoreDockingDemo.Web.Models.Common;

namespace WorkflowCoreDockingDemo.Web.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class EditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool HasPermission(FlatPermissionDto permission)
        {
            return GrantedPermissionNames.Contains(permission.Name);
        }
    }
}
