﻿using System.Collections.Generic;
using WorkflowCoreDockingDemo.Roles.Dto;

namespace WorkflowCoreDockingDemo.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
