﻿using System.Collections.Generic;
using WorkflowCoreDockingDemo.Roles.Dto;

namespace WorkflowCoreDockingDemo.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}