﻿using Abp.AutoMapper;
using WorkflowCoreDockingDemo.Sessions.Dto;

namespace WorkflowCoreDockingDemo.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}
