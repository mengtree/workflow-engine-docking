﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace WorkflowCoreDockingDemo.Web.Views
{
    public abstract class WorkflowCoreDockingDemoViewComponent : AbpViewComponent
    {
        protected WorkflowCoreDockingDemoViewComponent()
        {
            LocalizationSourceName = WorkflowCoreDockingDemoConsts.LocalizationSourceName;
        }
    }
}
