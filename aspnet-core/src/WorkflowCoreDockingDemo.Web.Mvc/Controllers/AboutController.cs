﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using WorkflowCoreDockingDemo.Controllers;

namespace WorkflowCoreDockingDemo.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : WorkflowCoreDockingDemoControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
