using System.Threading.Tasks;
using Abp.Web.Security.AntiForgery;
using Microsoft.AspNetCore.Antiforgery;
using WorkflowCoreDockingDemo.Controllers;
using Microsoft.AspNetCore.Mvc;
using WorkflowCoreDockingDemo.Sessions;
using Microsoft.AspNetCore.Http;

namespace WorkflowCoreDockingDemo.Web.Host.Controllers
{
    public class AntiForgeryController : WorkflowCoreDockingDemoControllerBase
    {
        private readonly IAntiforgery _antiforgery;
        private readonly IAbpAntiForgeryManager _antiForgeryManager;
        private readonly ISessionAppService _sessionAppService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AntiForgeryController(IAntiforgery antiforgery, IAbpAntiForgeryManager antiForgeryManager, ISessionAppService sessionAppService)
        {
            _antiforgery = antiforgery;
            _antiForgeryManager = antiForgeryManager;
            _sessionAppService = sessionAppService;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }

        public void SetCookie()
        {
            _antiForgeryManager.SetCookie(HttpContext);
        }
    }

   
}
