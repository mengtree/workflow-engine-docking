﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using WorkflowCoreDockingDemo.Configuration;

namespace WorkflowCoreDockingDemo.Web.Host.Startup
{
    [DependsOn(
       typeof(WorkflowCoreDockingDemoWebCoreModule))]
    public class WorkflowCoreDockingDemoWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public WorkflowCoreDockingDemoWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WorkflowCoreDockingDemoWebHostModule).GetAssembly());
        }
    }
}
