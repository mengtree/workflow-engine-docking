﻿using WorkflowCoreDockingDemo.Debugging;

namespace WorkflowCoreDockingDemo
{
    public class WorkflowCoreDockingDemoConsts
    {
        public const string LocalizationSourceName = "WorkflowCoreDockingDemo";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;


        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public static readonly string DefaultPassPhrase =
            DebugHelper.IsDebug ? "gsKxGZ012HLL3MI5" : "85d4eaba30de4a5994f0a605333bd3a6";
    }
}
