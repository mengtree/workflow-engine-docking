﻿using Abp.Dependency;
using Abp.Localization;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Runtime.Security;
using Abp.Timing;
using Abp.Zero;
using Abp.Zero.Configuration;
using System.Linq;
using WorkflowCoreDockingDemo.Authorization.Roles;
using WorkflowCoreDockingDemo.Authorization.Users;
using WorkflowCoreDockingDemo.Configuration;
using WorkflowCoreDockingDemo.CustomForm;
using WorkflowCoreDockingDemo.Localization;
using WorkflowCoreDockingDemo.MultiTenancy;
using WorkflowCoreDockingDemo.Timing;
using WorkflowCoreDockingDemo.Workflows;

namespace WorkflowCoreDockingDemo
{
    [DependsOn(typeof(AbpZeroCoreModule))]
    public class WorkflowCoreDockingDemoCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            // Declare entity types
            Configuration.Modules.Zero().EntityTypes.Tenant = typeof(Tenant);
            Configuration.Modules.Zero().EntityTypes.Role = typeof(Role);
            Configuration.Modules.Zero().EntityTypes.User = typeof(User);

            WorkflowCoreDockingDemoLocalizationConfigurer.Configure(Configuration.Localization);

            // Enable this line to create a multi-tenant application.
            Configuration.MultiTenancy.IsEnabled = WorkflowCoreDockingDemoConsts.MultiTenancyEnabled;

            // Configure roles
            AppRoleConfig.Configure(Configuration.Modules.Zero().RoleManagement);

            Configuration.Settings.Providers.Add<AppSettingProvider>();
            
            Configuration.Localization.Languages.Add(new LanguageInfo("fa", "فارسی", "famfamfam-flags ir"));
            
            Configuration.Settings.SettingEncryptionConfiguration.DefaultPassPhrase = WorkflowCoreDockingDemoConsts.DefaultPassPhrase;
            SimpleStringCipher.DefaultPassPhrase = WorkflowCoreDockingDemoConsts.DefaultPassPhrase;
        }

        public override void Initialize()
        {
            var types = typeof(WorkflowCoreDockingDemoCoreModule).GetAssembly().GetTypes().Where(t => t.IsClass && typeof(IWorkflowStatusUpdatable).IsAssignableFrom(t));

            foreach (var type in types)
            {
                try
                {
                    IocManager.RegisterIfNot(typeof(IWorkflowStatusUpdatable), type, DependencyLifeStyle.Transient);
                }
                catch (System.Exception)
                {
                    //
                }
            }

            IocManager.RegisterAssemblyByConvention(typeof(WorkflowCoreDockingDemoCoreModule).GetAssembly());
            IocManager.Register<WorkflowAdapter>();

           
        


        }

        public override void PostInitialize()
        {
            IocManager.Resolve<AppTimes>().StartupTime = Clock.Now;
        }
    }
}
