﻿using Abp.Domain.Repositories;
using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCoreDockingDemo.Workflows;

namespace WorkflowCoreDockingDemo.CustomForm
{
    public class CustomFormStore : IWorkflowStatusUpdatable
    {
        private readonly IRepository<LeaveForm, long> leaveFormRepository;

        public CustomFormStore(IRepository<LeaveForm, long> leaveFormRepository)
        {
            this.leaveFormRepository = leaveFormRepository;
        }

        public IEnumerable<BaseWorkTaskInfo> GetUpdatableWorkTasks()
        {
            return leaveFormRepository.GetAll()
                .Where(f => f.WorkflowStatus != WorkTaskStatus.Processed)
                .ToList()
                .Select(f=>new BaseWorkTaskInfo
                {
                    entityFullName=typeof(LeaveForm).FullName,
                    entityKeyValue = f.Id.ToString(),
                })
                .ToList();
        }

        public async Task UpdateWorkflowStatus(string entityFullName, string entityKeyValue, WorkTaskStatus workTaskStatus)
        {
            if (!typeof(LeaveForm).FullName.Equals(entityFullName)) return;
            var entity = leaveFormRepository.Get(long.Parse(entityKeyValue));
            if (entity == null) return;
            await entity.UpdateWorkflowStatus(workTaskStatus);
            await leaveFormRepository.UpdateAsync(entity);
        }
    }
}
