﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCoreDockingDemo.Workflows;

namespace WorkflowCoreDockingDemo.CustomForm
{
    public class LeaveForm : Entity<long>, IWorkflowStatus
    {
        public LeaveForm()
        {
        }

        public LeaveForm(int days, string reason, string memo)
        {
            Days = days;
            Reason = reason;
            Memo = memo;
            UpdateTime = DateTime.Now;
        }


        /// <summary>
        /// 天数
        /// </summary>
        public int Days { get; set; }
        /// <summary>
        /// 原因
        /// </summary>
        public string Reason { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }

        public DateTime UpdateTime { get; set; }
        public WorkTaskStatus WorkflowStatus { get; set; }
        public DateTime? WorkflowStatusUpdateTime { get; set; }

        public bool IsEditable()
        {
            return WorkflowStatusExtension.IsEditable(this);
        }
        public void Update(int days, string reason, string memo)
        {
            Days = days;
            Reason = reason;
            Memo = memo;
            UpdateTime = DateTime.Now;
        }
    }
}
