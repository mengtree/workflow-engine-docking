﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkflowCoreDockingDemo.Workflows
{
    public class TaskStateChangeData
    {
        public WorkTask WorkTask { get; set; }
        public WorkTaskStatus OldWorkTaskStatus { get; set; }
        public WorkTaskStatus NewWorkTaskStatus { get; set; }
    }
}
