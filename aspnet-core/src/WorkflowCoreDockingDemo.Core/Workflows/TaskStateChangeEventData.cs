﻿using Abp.Events.Bus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkflowCoreDockingDemo.Workflows
{
    public class TaskStateChangeEventData : TaskStateChangeData, IEventData
    {
        public DateTime EventTime { get; set; }
        public object EventSource { get; set; }
    }
}
