﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkflowCoreDockingDemo.Workflows
{
    public class WorkTask
    {
        /// 实体全称
        /// </summary>
        public string EntityFullName { get; set; }
        /// <summary>
        /// 实体主键值
        /// </summary>
        public string EntityKeyValue { get; set; }
        /// <summary>
        /// 审批状态
        /// </summary>
        public WorkTaskStatus WorkTaskStatus { get; set; }
    }
}
