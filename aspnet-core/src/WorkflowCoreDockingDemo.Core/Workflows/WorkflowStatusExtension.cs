﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkflowCoreDockingDemo.Workflows
{
    public static class WorkflowStatusExtension
    {
        public static bool IsEditable(this IWorkflowStatus workflow)
        {
            return workflow.WorkflowStatus == WorkTaskStatus.Pending;
        }
        public static async Task UpdateWorkflowStatus(this IWorkflowStatus workflow, WorkTaskStatus workflowStatus)
        {
            workflow.WorkflowStatus = workflowStatus;
            workflow.WorkflowStatusUpdateTime = DateTime.Now;
        }
    }
}
