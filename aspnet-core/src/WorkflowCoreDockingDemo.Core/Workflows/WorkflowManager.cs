﻿using Abp.Domain.Services;
using Abp.Events.Bus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCoreDockingDemo.CustomForm;

namespace WorkflowCoreDockingDemo.Workflows
{
    public class WorkflowManager:IDomainService
    {
        private readonly IEventBus eventBus;
        private readonly IEnumerable<IWorkflowStatusUpdatable> _WorkflowStatusUpdatables;

        public WorkflowManager(IEventBus eventBus, IEnumerable<IWorkflowStatusUpdatable> workflowStatusUpdatables)
        {
            this.eventBus = eventBus;
            _WorkflowStatusUpdatables = workflowStatusUpdatables;
        }

        public async Task HandleStatusChange(TaskStateChangeData taskStateChangeData)
        {
            //var eventData = new TaskStateChangeEventData
            //{
            //    WorkTask = taskStateChangeData.WorkTask,
            //    NewWorkTaskStatus = taskStateChangeData.NewWorkTaskStatus,
            //    OldWorkTaskStatus = taskStateChangeData.OldWorkTaskStatus,
            //    EventTime = DateTime.Now,
            //    EventSource = "WorkflowManager.HandleStatusChange"
            //};
            //await eventBus.TriggerAsync(eventData);

            foreach (var _WorkflowStatusUpdatable in _WorkflowStatusUpdatables)
            {
                await _WorkflowStatusUpdatable.UpdateWorkflowStatus(taskStateChangeData.WorkTask.EntityFullName, taskStateChangeData.WorkTask.EntityKeyValue, taskStateChangeData.NewWorkTaskStatus);
            }

            
        }
    }
}
