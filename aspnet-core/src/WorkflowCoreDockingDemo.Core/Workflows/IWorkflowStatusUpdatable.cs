﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkflowCoreDockingDemo.Workflows
{
    public interface IWorkflowStatusUpdatable
    {

        Task UpdateWorkflowStatus(string entityFullName, string entityKeyValue, WorkTaskStatus workTaskStatus);

        IEnumerable<BaseWorkTaskInfo> GetUpdatableWorkTasks();

    }


    public class BaseWorkTaskInfo
    {
        public string entityFullName { get;set; }
        public string entityKeyValue { get;set; }
    }

}
