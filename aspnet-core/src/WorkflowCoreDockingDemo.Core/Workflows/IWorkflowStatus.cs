﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkflowCoreDockingDemo.Workflows
{
    public interface IWorkflowStatus
    {
        public WorkTaskStatus WorkflowStatus { get; set; }
        public DateTime? WorkflowStatusUpdateTime { get; set; }

    }


    public enum WorkTaskStatus
    {
        /// <summary>
        /// 待处理
        /// </summary>
        Pending,
        /// <summary>
        /// 处理中
        /// </summary>
        Processing,
        /// <summary>
        /// 已完成
        /// </summary>
        Processed,
        /// <summary>
        /// 撤销
        /// </summary>
        Canceled

    }
}
