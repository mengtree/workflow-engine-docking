﻿using Abp.MultiTenancy;
using WorkflowCoreDockingDemo.Authorization.Users;

namespace WorkflowCoreDockingDemo.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
