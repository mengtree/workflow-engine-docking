﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace WorkflowCoreDockingDemo.Localization
{
    public static class WorkflowCoreDockingDemoLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(WorkflowCoreDockingDemoConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(WorkflowCoreDockingDemoLocalizationConfigurer).GetAssembly(),
                        "WorkflowCoreDockingDemo.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
