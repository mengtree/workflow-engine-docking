﻿using Abp.Authorization;
using WorkflowCoreDockingDemo.Authorization.Roles;
using WorkflowCoreDockingDemo.Authorization.Users;

namespace WorkflowCoreDockingDemo.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
