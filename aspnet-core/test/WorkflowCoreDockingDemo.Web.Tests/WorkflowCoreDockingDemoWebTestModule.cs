﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using WorkflowCoreDockingDemo.EntityFrameworkCore;
using WorkflowCoreDockingDemo.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace WorkflowCoreDockingDemo.Web.Tests
{
    [DependsOn(
        typeof(WorkflowCoreDockingDemoWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class WorkflowCoreDockingDemoWebTestModule : AbpModule
    {
        public WorkflowCoreDockingDemoWebTestModule(WorkflowCoreDockingDemoEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WorkflowCoreDockingDemoWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(WorkflowCoreDockingDemoWebMvcModule).Assembly);
        }
    }
}