import Entity from './entity'

export default class leaveForm extends Entity<number>{
    days:number;
    reason:Date;
    memo:string;
    workflowStatus:number;
    workflowStatusName:string;
    UpdateTime:Date; 

    public getStatusName() :string{
        switch (this.workflowStatus) {
          case 0:
            return "待发起";
          case 1:
            return "审批中";
          case 2:
            return "已审批";
          default:
            return this.workflowStatus+"";
        }
      }
}