import {Store,Module,ActionContext} from 'vuex'
import ListModule from './list-module'
import ListState from './list-state'
import LeaveForm from '../entities/leaveForm'
import Ajax from '../../lib/ajax'
import thirdpartajax from '../../lib/thirdpartajax'
import PageResult from '@/store/entities/page-result';
import ListMutations from './list-mutations'


const workflowHost =  process.env.NODE_ENV === 'production' ? 'http://39.101.74.14:8083/' : 'http://localhost:5000/';
interface LeaveFormState extends ListState<LeaveForm>{
    editLeaveForm:LeaveForm;
}
class LeaveFormModule extends ListModule<LeaveFormState,any,LeaveForm>{

    

    state={
        totalCount:0,
        currentPage:1,
        pageSize:10,
        list: new Array<LeaveForm>(),
        loading:false,
        editLeaveForm:new LeaveForm()
    }
    actions={
        
        async getAll(context:ActionContext<LeaveFormState,any>,payload:any){
            context.state.loading=true;
            let reponse=await Ajax.get('/api/services/app/LeaveForm/GetAll',{params:payload.data});
            context.state.loading=false;
            let page=reponse.data.result as PageResult<LeaveForm>;
            context.state.totalCount=page.totalCount;

            let getStatusName=function(workflowStatus:number) :string{
                switch (workflowStatus) {
                  case 0:
                    return "待发起";
                  case 1:
                    return "审批中";
                  case 2:
                    return "已审批";
                  default:
                    return workflowStatus+"";
                }
              }


            page.items.forEach(item=>{
                item.workflowStatusName = getStatusName(item.workflowStatus)
            })
            context.state.list=page.items;
        },
        async create(context:ActionContext<LeaveFormState,any>,payload:any){
            await Ajax.post('/api/services/app/LeaveForm/Create',payload.data);
        },
        async update(context:ActionContext<LeaveFormState,any>,payload:any){
            await Ajax.put('/api/services/app/LeaveForm/Update',payload.data);
        },
        async delete(context:ActionContext<LeaveFormState,any>,payload:any){
            await Ajax.delete('/api/services/app/LeaveForm/Delete?Id='+payload.data.id);
        },
        async get(context:ActionContext<LeaveFormState,any>,payload:any){
            let reponse=await Ajax.get('/api/services/app/LeaveForm/Get?Id='+payload.id);
            return reponse.data.result as LeaveForm;
        },
        async startWorkTask(context:ActionContext<LeaveFormState,any>,payload:any){
            return await thirdpartajax.post(workflowHost+'api/WorkFlow/StartWorkTask',payload.data);
        },
        async createWorkTask(context:ActionContext<LeaveFormState,any>,payload:any){
            return await thirdpartajax.post(workflowHost+'api/WorkFlow/CreateWorkTaskWithDefaultVersion',payload.data);
        },
    };
    mutations={
        setCurrentPage(state:LeaveFormState,page:number){
            state.currentPage=page;
        },
        setPageSize(state:LeaveFormState,pagesize:number){
            state.pageSize=pagesize;
        },
        edit(state:LeaveFormState,leaveForm:LeaveForm){
            state.editLeaveForm=leaveForm;
        }
    }
}
const leaveFormModule=new LeaveFormModule();
export default leaveFormModule;